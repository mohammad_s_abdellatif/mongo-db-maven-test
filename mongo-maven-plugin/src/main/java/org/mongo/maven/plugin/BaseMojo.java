package org.mongo.maven.plugin;

import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public abstract class BaseMojo extends AbstractMojo {
    /**
     * Location of the file.
     */
    @Parameter(defaultValue = "${project.build.directory}/mongo", property = "outputDir", required = true)
    protected File workingDiretory;

    @Parameter(defaultValue = "${project.build.directory}/mongo/data")
    protected File dbPath;

    @Parameter(defaultValue = "37017", required = false)
    protected int port;

    @Parameter(defaultValue = "${localRepository}", readonly = true)
    protected ArtifactRepository localRepository;

    @Parameter(defaultValue = "${mojo}", readonly = true)
    protected MojoExecution mojoExecution;

    @Component
    protected ArtifactFactory artifactFactory;

    @Parameter(defaultValue = "false", property = "mongo.plugin.skip")
    protected boolean skip;

    protected File getBinPath() {
        return new File(workingDiretory, "bin");
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skip) {
            getLog().info("skip execution");
            return;
        }
        doExecute();
    }

    protected static boolean isPosix() {
        String os = System.getProperty("os.name");
        return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0 || os.indexOf("sunos") >= 0);
    }

    protected abstract void doExecute() throws MojoExecutionException, MojoFailureException;

    protected void executeMongoCommand(String[] mongoCommand)
            throws IOException, MojoExecutionException, MojoFailureException {
        getLog().info("execute command: [" + debug(mongoCommand) + "]");
        if (isPosix()) {
            execute(new String[]{"bash", "-c", "chmod -R 777 *"}, null, getBinPath().getParentFile());
        }
        execute(mongoCommand, null, getBinPath());
    }

    protected void execute(String[] command, Map<String, String> env, File pwd)
            throws IOException, MojoFailureException, MojoExecutionException {
        getLog().info("execute command: " + debug(command));
        getLog().info("use environment: " + env);
        getLog().info("pwd is: " + pwd);
        ProcessBuilder builder = new ProcessBuilder(command);
        if (env != null)
            builder.environment().putAll(env);
        builder.directory(pwd);
        builder.redirectErrorStream(true);
        Process process = builder.start();
        try {
            int error = process.waitFor();
            if (error != 0)
                throw new MojoFailureException("failed while executing command");
        } catch (InterruptedException e) {
            throw new MojoExecutionException("interrupted while waiting for mongod to start");
        }
    }

    private String debug(String[] mongoCommand) {
        StringBuilder sb = new StringBuilder();
        for (String command : mongoCommand)
            sb.append(command).append(" ");
        return sb.toString();
    }

    protected String[] createMongodCommand() throws IOException {
        return new String[]{"./mongod", "--fork", "--logpath", getLogPath().toString(),
                "--port", port + "", "--dbpath", dbPath.getCanonicalPath()};
    }

    protected File getLogPath() {
        return new File(workingDiretory, "mongod.log");
    }

    public void setMojoExecution(MojoExecution mojoExecution) {
        this.mojoExecution = mojoExecution;
    }

    public void setWorkingDiretory(File workingDiretory) {
        this.workingDiretory = workingDiretory;
    }

    public void setDbPath(File dbPath) {
        this.dbPath = dbPath;
    }

    public void setLocalRepository(ArtifactRepository localRepository) {
        this.localRepository = localRepository;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setArtifactFactory(ArtifactFactory artifactFactory) {
        this.artifactFactory = artifactFactory;
    }
}
