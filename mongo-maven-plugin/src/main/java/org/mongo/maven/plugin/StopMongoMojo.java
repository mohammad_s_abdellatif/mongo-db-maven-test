package org.mongo.maven.plugin;

import java.io.File;
import java.io.IOException;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Goal which touches a timestamp file.
 *
 */
@Mojo(name = "stop")
public class StopMongoMojo extends BaseMojo {
	/**
	 * Location of the file.
	 */
	@Parameter(defaultValue = "${project.build.directory}/mongo", property = "outputDir", required = true)
	private File workingDiretory;

	public void doExecute() throws MojoExecutionException, MojoFailureException {
		try {
			String[] command = createMongodCommand();
			String[] shutdownCommand = new String[command.length + 1];
			System.arraycopy(command, 0, shutdownCommand, 0, command.length);
			shutdownCommand[command.length] = "--shutdown";
			executeMongoCommand(shutdownCommand);
		} catch (IOException e) {
			throw new MojoExecutionException("failed while trying to stop mongod", e);
		}
	}
}
