package org.mongo.maven.plugin;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtility {

	private static final int BUFFER_SIZE = 4096;

	public static void unzip(File zipFilePath, File destDirectory) throws IOException {
		FileInputStream in = new FileInputStream(zipFilePath);
		try {
			unzip(in, destDirectory);
		} finally {
			in.close();
		}
	}

	private static void unzip(InputStream in, File destDirectory) throws IOException {
		if (!destDirectory.exists())
			destDirectory.mkdirs();
		ZipInputStream zipIn = new ZipInputStream(in);
		ZipEntry entry = zipIn.getNextEntry();
		// iterates over entries in the zip file
		while (entry != null) {
			File entryPath = new File(destDirectory, entry.getName());
			if (!entry.isDirectory()) {
				// if the entry is a file, extracts it
				extractFile(zipIn, entryPath);
			} else {
				// if the entry is a directory, make the directory
				entryPath.mkdirs();
			}
			zipIn.closeEntry();
			entry = zipIn.getNextEntry();
		}
	}

	private static void extractFile(ZipInputStream zipIn, File filePath) throws IOException {
		File parentPath = filePath.getParentFile();
		if (!parentPath.exists()) {
			parentPath.mkdirs();
		}
		OutputStream outputStream = new FileOutputStream(filePath);
		try {
			BufferedOutputStream bos = new BufferedOutputStream(outputStream);
			byte[] bytesIn = new byte[BUFFER_SIZE];
			int read = 0;
			while ((read = zipIn.read(bytesIn)) != -1) {
				bos.write(bytesIn, 0, read);
			}
			bos.flush();
		} finally {
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
