package org.mongo.maven.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Goal which touches a timestamp file.
 *
 */
@Mojo(name = "start")
public class StartMongoMojo extends BaseMojo {

	public void doExecute() throws MojoExecutionException, MojoFailureException {
		getLog().info("extract mongod installation to path: " + super.workingDiretory);
		Artifact mongodArtifact = findSuitableArtifact();
		getLog().info("extrct artifact: " + mongodArtifact);
		File artifactFile = mongodArtifact.getFile();
		try {
			if (!workingDiretory.exists())
				workingDiretory.mkdirs();
			ZipUtility.unzip(artifactFile, workingDiretory);
		} catch (IOException e) {
			throw new MojoFailureException("failed while extracting mongo installation", e);
		}

		if (!dbPath.exists())
			dbPath.mkdirs();
		try {
			String[] mongoCommand = createStartCommand();
			executeMongoCommand(mongoCommand);
		} catch (IOException e) {
			dumpMongoLogsToLog();
			throw new MojoFailureException("failed while running mongod", e);
		}
	}

	private void dumpMongoLogsToLog() {
		File logPath = getLogPath();
		try {
			InputStream in = new FileInputStream(logPath);
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String line;
				while ((line = reader.readLine()) != null)
					getLog().info(line);
			} finally {
				in.close();
			}
		} catch (IOException e) {
			getLog().warn("unable to dump mongo log", e);
		}
	}

	@SuppressWarnings("deprecation")
	private Artifact findSuitableArtifact() {
		Artifact mongodArtifact;
		if (isPosix()) {
			mongodArtifact = artifactFactory.createArtifactWithClassifier(mojoExecution.getGroupId(),
					"mongo-installation", mojoExecution.getVersion(), "zip", "posix");
		} else {
			// windows
			mongodArtifact = artifactFactory.createArtifactWithClassifier(mojoExecution.getGroupId(),
					"mongo-installation", mojoExecution.getVersion(), "zip", "win");
		}
		return super.localRepository.find(mongodArtifact);
	}

	private String[] createStartCommand() throws IOException {
		String[] javaCommand = createMongodCommand();
		String[] command = javaCommand;

//		if (isPosix()) {
//			command = new String[javaCommand.length + 1];
//			System.arraycopy(javaCommand, 0, command, 0, javaCommand.length);
//			command = new String[] { "bash", "-c", String.join(" ", javaCommand) };
//		}
		return command;
	}

	

}
